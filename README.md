# itree - interactive tree
itree is a CLI tool for interactively viewing a directory hierarchy and optionally selecting a set of paths. The paths are simply written to a file with which you can do anything. I started itree because the performance of most filemanagers is unsatisfactory and most filemanagers don't offer the tree-like view I want. itree was influenced by nnn, broot, tree, vscode, ls, and the filemanager plugin for the micro editor. itree aims to offers the experience of micro/filemanager with better performance and without being coupled to micro.

## goals:
- performant
- efficient
- compact, nice, tree-like visualization

## non goals:
- to be perfect as this would be the enemy of the good
- be a replacement for a specific filemanager
- be a fully fledged filemanager with eg. previews.

## usecases:
- change directory with an alias similar to `itree && cd $(cat /tmp/paths)`
