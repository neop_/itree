#include "ncurses.h"

#include <memory>
#include <unordered_set>
#include <filesystem>
#include <vector>
#include <ranges>
#include <iterator>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <execution>
#include <cassert>
#include <type_traits>

#define ever (;;)

#if __has_builtin(__builtin_trap)
#	define TRAP() __builtin_trap()
#elif STATIC_ANALYSIS
#	define TRAP() (*((int*)0)=0)
#else
#	define TRAP() std::abort()
#endif

namespace fs = std::filesystem;
namespace ranges = std::ranges;
namespace views = std::views;


namespace c154b5f2_f5f1_44f3_bc3c_934c356295f5 {
	auto init() {
		// needs to be called before initscr
    	std::setlocale(LC_ALL, "");

		std::unique_ptr<WINDOW, decltype([](WINDOW*) {
			endwin();
		})> window{initscr()};

		// need to be called after initscr
		cbreak();
		noecho();

		nl();
		intrflush(stdscr, FALSE);
		keypad(stdscr, TRUE);
		// scrollok(window.get(), true);

		curs_set(0);

		return window;
	}

	auto get_win_rows() {
		return getmaxy(stdscr) - 1;
	}
} // namespace nc

namespace nc = c154b5f2_f5f1_44f3_bc3c_934c356295f5;

namespace neop {
	template <std::input_iterator I, std::sentinel_for<I> S>
	auto cut(auto& container, I&& begin, S&& end) {
		container.erase(
			ranges::move(std::forward<I>(begin), std::forward<S>(end), ranges::begin(container)).out/*<-new_end*/,
			ranges::end(container)
		);
	}
	template <std::input_iterator I, std::sentinel_for<I> S>
	auto cut(std::vector<bool>& container, I&& begin, S&& end) {
		// does not invalidate iterators before end:
		container.erase(end, container.end());
		container.erase(container.begin(), begin);
	}

	auto wrap_if(bool condition, auto&& pre, auto&& post, auto&& f) {
		if (condition) {
			pre();
		}

		f();

		if (condition) {
			post();
		}
	}
} // namespace neop


namespace {
using idx_t = decltype(0z);

struct PathHash {
	std::size_t operator()(fs::path const& p) const noexcept {
		return fs::hash_value(p);
	}
};

auto fname_of(const fs::path& path, const fs::file_status& status) {
	auto last_fname = *std::prev(path.end());
	if (fs::is_directory(status)) {
		last_fname /= "";
	}
	return last_fname;
}

auto fname_of(const fs::directory_entry& entry) {
	return fname_of(entry.path(), entry.status());
}

struct DirectoryTree;
void expand_current_folder(DirectoryTree& directory_tree, idx_t selection);

struct DirectoryTree {
	explicit DirectoryTree() {
		paths.push_back(".");
		levels.push_back(0);
		expanded.push_back(false);
		is_last_sibling.push_back(true);
		statuses.push_back(fs::status("."));

		expand_current_folder(*this, 0);

		assert(invariant());
	}
	std::vector<fs::path> paths;
	std::vector<int> levels;
	std::vector<bool> expanded;
	std::vector<bool> is_last_sibling;
	std::vector<fs::file_status> statuses;

	bool invariant() const {
		const auto common_size = ranges::size(paths);
		if (
			ranges::size(levels         ) != common_size or
			ranges::size(expanded       ) != common_size or
			ranges::size(is_last_sibling) != common_size or
			ranges::size(statuses       ) != common_size
		) {
			std::cerr << paths.size() << levels.size() << expanded.size() << is_last_sibling.size() << statuses.size();
			return false;
		};
		return true;
	}
};

struct State {
	idx_t selection = 0;
	DirectoryTree directory_tree;
};

static_assert(std::is_nothrow_move_assignable_v<State>);

auto ssize(const DirectoryTree& directory_tree) {
	return ssize(directory_tree.paths);
}

namespace path_color {
	enum : short {
		directory = 1,
		neutral,
		symlink,
		block,
		character,
		fifo,
		socket,
	};
}

auto init_colors() {
	constexpr static auto color_terminal_bg_default = -1;
	start_color();
	use_default_colors();

	init_pair(path_color::directory ,COLOR_BLUE        ,color_terminal_bg_default);
	init_pair(path_color::neutral   ,COLOR_WHITE       ,color_terminal_bg_default);
	init_pair(path_color::symlink   ,COLOR_CYAN        ,color_terminal_bg_default);
	init_pair(path_color::block     ,COLOR_YELLOW      ,color_terminal_bg_default);
	init_pair(path_color::character ,COLOR_YELLOW      ,color_terminal_bg_default);
	init_pair(path_color::fifo      ,COLOR_YELLOW      ,color_terminal_bg_default);
	init_pair(path_color::socket    ,COLOR_MAGENTA     ,color_terminal_bg_default);
}

auto color_for_status(const fs::file_status& status) {
	using enum fs::file_type;
	switch (status.type()) {
		case regular:   return path_color::neutral;
		case directory: return path_color::directory;
		case symlink:   return path_color::symlink;
		case block:     return path_color::block;
		case character: return path_color::character;
		case fifo:      return path_color::fifo;
		case socket:    return path_color::socket;
		// TODO: not_found returned for existing symlink; explore
		case not_found: return path_color::neutral;
		// case unknown:   std::cerr << "unknown\n";   return path_color::neutral;
		// case none:      std::cerr << "none\n";      return path_color::neutral;
		default: TRAP();
	}
}

auto calc_indentation_for(const DirectoryTree& directory_tree, idx_t next) {
	std::vector<const char *> indentation;
	for (auto i = 1; i < next; ++i) {
		indentation.resize(directory_tree.levels[i]-1);
		indentation.emplace_back(directory_tree.is_last_sibling[i] ? "   " : "│  ");
	}
	return indentation;
}

auto draw_real_directories(const DirectoryTree& directory_tree, idx_t selection, idx_t line_render_count) {
	assert(directory_tree.invariant());

	const auto low = std::max(std::min(ssize(directory_tree) - line_render_count, selection - line_render_count / 2), 0z);
	const auto high = std::min(ssize(directory_tree), low + line_render_count);

	auto indentation = calc_indentation_for(directory_tree, low);
	for (auto i = low; i < high; ++i) {
		color_set(path_color::neutral, nullptr);

		if (i != 0) {
			addstr("   ");
			// TODO: assert resize always shrinks
			indentation.resize(directory_tree.levels[i] - 1);
			for (const auto& part : indentation) addstr(part);
			indentation.emplace_back(directory_tree.is_last_sibling[i] ? "   " : "│  ");

			addstr(directory_tree.is_last_sibling[i] ? "└─ " : "├─ ");
		} else {
			addstr("└─ ");
		}

		neop::wrap_if(i == selection, standout, standend, [&]{
			color_set(color_for_status(directory_tree.statuses[i]), nullptr);
			addstr(directory_tree.paths[i].lexically_normal().c_str());
			addch('\n');
		});
	}
}

auto draw(const DirectoryTree& directory_tree, idx_t selection) {
	assert(directory_tree.invariant());
	erase();

	const auto rows = nc::get_win_rows();
	color_set(path_color::neutral, nullptr);

	const auto parent_visible = std::min(ssize(directory_tree) - (rows - 1), selection - (rows - 1) / 2) < 0;
	if (parent_visible) {
		neop::wrap_if(selection == -1, standout, standend, [&]{
			addstr("..\n");
		});
	}

	draw_real_directories(directory_tree, selection, rows - int(parent_visible));

	refresh();
}

auto get_full_path(const DirectoryTree& directory_tree, const idx_t selection) {
	assert(selection >= 0);
	assert(directory_tree.invariant());

	fs::path full_path = directory_tree.paths[selection];
	auto depth = directory_tree.levels[selection];
	for (auto i = selection - 1; i >= 0; --i) {
		if (directory_tree.levels[i] == depth - 1) {
			full_path = directory_tree.paths[i] / full_path;
			--depth;
		}
	}
	return full_path;
}

auto output_selected_files(const DirectoryTree& directory_tree, const idx_t selection) {
	// TODO: not ensured:
	assert(selection >= 0);
	assert(directory_tree.invariant());

	std::ofstream paths_file("/tmp/paths");
	//TODO: fs::absolute_path
	paths_file << fs::current_path() / get_full_path(directory_tree, selection).lexically_normal().c_str() << '\n';
}

void expand_current_folder(DirectoryTree& directory_tree, const idx_t selection) {
	assert(directory_tree.invariant());

	if (selection < 0 or directory_tree.expanded[selection]) {
		return;
	}
	directory_tree.expanded[selection] = true;

	{
		const auto previous_size = ssize(directory_tree);

		for (auto&& entry : fs::directory_iterator(get_full_path(directory_tree, selection))) {
			// TODO: is_hidden(fname)
			//if (fname.native().front() == '.') continue;
			directory_tree.paths.push_back(fname_of(entry));
			directory_tree.statuses.push_back(entry.status());
		}

		std::rotate(std::execution::par_unseq, directory_tree.paths   .begin() + selection + 1, directory_tree.paths   .begin() + previous_size, directory_tree.paths   .end());
		std::rotate(std::execution::par_unseq, directory_tree.statuses.begin() + selection + 1, directory_tree.statuses.begin() + previous_size, directory_tree.statuses.end());
	}
	const auto folder_size = ssize(directory_tree.paths) - ssize(directory_tree.levels);

	directory_tree.levels.insert(
		directory_tree.levels.begin() + selection + 1,
		folder_size,
		directory_tree.levels[selection] + 1
	);
	directory_tree.expanded.insert(
		directory_tree.expanded.begin() + selection + 1,
		folder_size,
		false
	);

	// fastest way because of vector<bool>
	if (folder_size > 0) {
		directory_tree.is_last_sibling.insert(
			directory_tree.is_last_sibling.begin() + selection + 1,
			1,
			true
		);
		directory_tree.is_last_sibling.insert(
			directory_tree.is_last_sibling.begin() + selection + 1,
			folder_size - 1,
			false
		);
	}

	assert(directory_tree.invariant());
}


auto current_folder_expanded(DirectoryTree directory_tree, const idx_t selection) {
	expand_current_folder(directory_tree, selection);
	return directory_tree;
}


auto collapse_current_folder(DirectoryTree& directory_tree, idx_t selection) noexcept {
	assert(directory_tree.invariant());

	if (selection < 0 or not directory_tree.expanded[selection]) {
		return;
	}
	directory_tree.expanded[selection] = false;

	const auto next_parent_idx = std::find_if(
		directory_tree.levels.begin() + selection + 1,
		directory_tree.levels.end(),
		[selected_level = directory_tree.levels[selection]](auto level) { return level <= selected_level; }
	) - directory_tree.levels.begin();

	directory_tree.paths          .erase(directory_tree.paths          .begin() + selection + 1, directory_tree.paths          .begin() + next_parent_idx);
	directory_tree.levels         .erase(directory_tree.levels         .begin() + selection + 1, directory_tree.levels         .begin() + next_parent_idx);
	directory_tree.expanded       .erase(directory_tree.expanded       .begin() + selection + 1, directory_tree.expanded       .begin() + next_parent_idx);
	directory_tree.is_last_sibling.erase(directory_tree.is_last_sibling.begin() + selection + 1, directory_tree.is_last_sibling.begin() + next_parent_idx);
	directory_tree.statuses       .erase(directory_tree.statuses       .begin() + selection + 1, directory_tree.statuses       .begin() + next_parent_idx);

	assert(directory_tree.invariant());
}

auto focus_parent(DirectoryTree& directory_tree) {
	assert(directory_tree.invariant());

	directory_tree.paths.front() = fname_of(fs::current_path(), directory_tree.statuses.front());
	for (auto& level : directory_tree.levels) {
		++level;
	}

	fs::current_path("..");

	directory_tree.paths          .insert(directory_tree.paths          .begin(), ".");
	directory_tree.statuses       .insert(directory_tree.statuses       .begin(), fs::status(directory_tree.paths.front()));
	directory_tree.levels         .insert(directory_tree.levels         .begin(), 0);
	directory_tree.expanded       .insert(directory_tree.expanded       .begin(), true);
	directory_tree.is_last_sibling.insert(directory_tree.is_last_sibling.begin(), true);


	for (auto&& entry : fs::directory_iterator(directory_tree.paths.front())) {
		// TODO: is_hidden(fname)
		auto fname = fname_of(entry);
		if (fname == directory_tree.paths[1]) continue;
		directory_tree.paths.push_back(std::move(fname));
		directory_tree.statuses.push_back(entry.status());
	}
	const auto folder_size = ssize(directory_tree.paths) - ssize(directory_tree.levels);

	directory_tree.is_last_sibling[1] = folder_size == 0;

	directory_tree.levels  .insert(directory_tree.levels  .end(), folder_size, 1);
	directory_tree.expanded.insert(directory_tree.expanded.end(), folder_size, false);
	if (folder_size > 0) {
		directory_tree.is_last_sibling.insert(
			directory_tree.is_last_sibling.end(),
			folder_size - 1,
			false
		);
		directory_tree.is_last_sibling.insert(
			directory_tree.is_last_sibling.end(),
			1,
			true
		);
	}

	assert(directory_tree.invariant());
}

auto focus_subfolder(DirectoryTree& directory_tree, idx_t selection) {
	assert(selection >= 0);

	const auto next_parent_idx = std::find_if(
		directory_tree.levels.begin() + selection + 1,
		directory_tree.levels.end(),
		[selected_level = directory_tree.levels[selection]](auto level) { return level <= selected_level; }
	) - directory_tree.levels.begin();

	fs::current_path(get_full_path(directory_tree, selection));
	directory_tree.paths[selection] = ".";

	neop::cut(directory_tree.paths          , directory_tree.paths          .begin() + selection, directory_tree.paths          .begin() + next_parent_idx);
	auto beginning_of_end = std::transform(   directory_tree.levels         .begin() + selection, directory_tree.levels         .begin() + next_parent_idx,
		directory_tree.levels.begin(),
		[selected_level = directory_tree.levels[selection]](auto level) {
			return level - selected_level;
		}
	);
	directory_tree.levels.erase(beginning_of_end, directory_tree.levels.end());
	neop::cut(directory_tree.expanded       , directory_tree.expanded       .begin() + selection, directory_tree.expanded       .begin() + next_parent_idx);
	neop::cut(directory_tree.is_last_sibling, directory_tree.is_last_sibling.begin() + selection, directory_tree.is_last_sibling.begin() + next_parent_idx);
	neop::cut(directory_tree.statuses       , directory_tree.statuses       .begin() + selection, directory_tree.statuses       .begin() + next_parent_idx);
}

auto focus_current_folder(State state) {
	auto& selection = state.selection;
	auto& directory_tree = state.directory_tree;
	assert(directory_tree.invariant());

	if (selection < 0) {
		focus_parent(directory_tree);
	} else {
		focus_subfolder(directory_tree, selection);
	}

	selection = 0;

	assert(directory_tree.invariant());
	return state;
}

auto limit_upper(idx_t desired, idx_t tree_size) {
	return std::min(desired, tree_size - 1);
}

auto limit_lower(idx_t desired) {
	return std::max(-1z, desired);
}


auto process_event(State& state, int input) {
	auto& selection = state.selection;
	auto& directory_tree = state.directory_tree;
	try { switch (input) {
		case KEY_DOWN:
		selection = limit_upper(selection + 1,                  ssize(directory_tree));
		break;

		case KEY_NPAGE:
		selection = limit_upper(selection + nc::get_win_rows(), ssize(directory_tree));
		break;

		case KEY_UP:
		selection = limit_lower(selection - 1);
		break;

		case KEY_PPAGE:
		selection = limit_lower(selection - nc::get_win_rows());
		break;

		case KEY_RIGHT:
		directory_tree = current_folder_expanded(directory_tree, selection);
		break;

		case KEY_LEFT:
		collapse_current_folder(directory_tree, selection);
		break;

		case '\n':
		state = focus_current_folder(state);
		break;

		case 'p':
		output_selected_files(directory_tree, selection);
		break;
	}} catch(const std::exception& e) {
		std::cerr << e.what() << '\n';
	}

	draw(directory_tree, selection);
}
} // namespace



int main() try {
	auto window = nc::init();

	init_colors();

	State state;

	draw(state.directory_tree, state.selection);

	for ever {
		const auto input = getch();
		if (input == 'q') {
			output_selected_files(state.directory_tree, state.selection);
			return EXIT_SUCCESS;
		}
		process_event(state, input);
	}
} catch (const std::exception& e) {
	std::puts(e.what());
	return EXIT_FAILURE;
}

