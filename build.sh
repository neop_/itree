#!/bin/sh
mkdir -p build
cd build

CXX="${CXX:-ccache g++}"

echo ../src/*.cpp | xargs -n 1 -P 100 $CXX \
	-Wall \
	-Wextra \
	-std=c++2b \
	-fcompare-debug-second \
	-g \
	-c \
&& g++ *.o -lncurses -ltbb -o ../main